import {Component, OnInit, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {WeatherService} from '../shared/weather/weather.service';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {
  message: any;
  address_in: any;

  constructor(@Inject(DOCUMENT) private document: any, private weatherService: WeatherService) {
    this.address_in = document.getElementById('address').value;
    if (this.address_in === undefined || this.address_in === '') {
      this.address_in = 'Los Angeles, Ca';
    }
    console.log('localtion=>'+this.address_in);
    this.weatherService.setAddress(this.address_in);
  }

  ngOnInit() {
    this.weatherService.getAll().subscribe(message => {
      this.message = message;
      console.log('this.message =' + message);
    });
  }
}
