import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})

export class WeatherService {
  address: any;
  apiRoot: string = 'https://tomweatherdemo.appspot.com/api/weather/pastweek/';

  constructor(private http: HttpClient) {
  }

  public setAddress(val: any): void {
    this.address = val;
  }

  getAll(): Observable<any> {
    var urlRequest = this.apiRoot + this.address + '/';
    return this.http.get(urlRequest);
  }
}
