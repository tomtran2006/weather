= Weather Micro Service app

== Getting started

1) To use this Weather app, you need to register a free account with Dark Sky API (https://darksky.net/dev/) to get [API Key] on the

You can create an `application.properties` in `src/main/resources` and add your
API key there:

weather.api.darkskykey=xxxxxxxxxxxx

2) To use this Weather app, you also need to register a free account with google map API (https://developers.google.com/maps/documentation/) to get [API Key] on the

You can create an `application.properties` in `src/main/resources` and add your
API key there:

weather.api.googlemapkey=xxxxxxxxxxxxxxxx

3) For start in local environment ( require maven and java jdk/jre installed in your machine)
$ mvn spring-boot:run

NOTES: There is a completed end to end this micro service and front end are up and running in google cloud platform

 https://angular-app-weather-demo.appspot.com


 Also, you could test the Rest API service just provide location address ( full or just city , state e.g Houston , TX)

  https://tomweatherdemo.appspot.com/api/weather/pastweek/{location-address}

  e.g  https://tomweatherdemo.appspot.com/api/weather/pastweek/los angeles , ca/

Notes: defaul location address will Los Angeles , Ca"