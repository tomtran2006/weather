package com.tom.weather.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.tom.weather.WeatherAppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class GeoMapService {
    private static final Logger logger = LoggerFactory.getLogger(GeoMapService.class);
    private final String apiKey;

    public GeoMapService(WeatherAppProperties properties) {
        this.apiKey = properties.getApi().getGooglemapKey();
    }

    @Cacheable("geomapinfo")
    public GeoMapInfo getGeoMapInfoByLocation(String location) {
        logger.info("Requesting Geo Map location  = {} ", location);
        GeoMapInfo geoMapInfo = null;
        try {
            GeoApiContext context = new GeoApiContext.Builder().apiKey(this.apiKey).build();
            GeocodingResult[] results = GeocodingApi.geocode(context, location).await();
            //Gson gson = new GsonBuilder().setPrettyPrinting().create();
            final double lat = results[0].geometry.location.lat;
            final double lon = results[0].geometry.location.lng;
            geoMapInfo = new GeoMapInfo(lon, lat);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return geoMapInfo;
    }
}
