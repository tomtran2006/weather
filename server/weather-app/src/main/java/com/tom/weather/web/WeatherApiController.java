package com.tom.weather.web;

import com.tom.weather.service.GeoMapInfo;
import com.tom.weather.service.GeoMapService;
import com.tom.weather.service.WeatherService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.plogitech.darksky.forecast.model.Forecast;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@RestController
@RequestMapping("/api/weather")
@CrossOrigin(origins = "https://angular-app-weather-demo.appspot.com")
public class WeatherApiController {

    private final WeatherService weatherService;
    private final GeoMapService geoMapService;

    public WeatherApiController(WeatherService weatherService, GeoMapService geoMapService) {
        this.weatherService = weatherService;
        this.geoMapService = geoMapService;
    }

    @RequestMapping("/pastweek/{location}")
    public Forecast getWeatherPastweek(@PathVariable String location) {
        Forecast forecast = null;
        Optional<GeoMapInfo> geoMapInfo = Optional.ofNullable(geoMapService.getGeoMapInfoByLocation(location));
        if (geoMapInfo.isPresent()) {
            Instant pastTime = Instant.now().minus(7, ChronoUnit.DAYS);
            return this.weatherService.getHistoricalWeather(pastTime, geoMapInfo.get().getLongitude(), geoMapInfo.get().getLatitude());
        }
        return forecast;
    }
}
