package com.tom.weather;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@ConfigurationProperties("weather")
public class WeatherAppProperties {

    @Valid
    private final Api api = new Api();

    public Api getApi() {
        return this.api;
    }

    public static class Api {

        /**
         * API key of the Dark Sky Weather service.
         */
        @NotNull
        private String darkskykey;

        public String getDarkskykey() {
            return this.darkskykey;
        }

        public void setDarkskykey(String darkskykey) {
            this.darkskykey = darkskykey;
        }


        /**
         * API key of the Google map  service.
         */
        @NotNull
        private String googlemapkey;

        public String getGooglemapKey() {
            return this.googlemapkey;
        }

        public void setGooglemapkey(String googlemapkey) {
            this.googlemapkey = googlemapkey;
        }

    }

}
