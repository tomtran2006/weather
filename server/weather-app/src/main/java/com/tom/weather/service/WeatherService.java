package com.tom.weather.service;

import com.tom.weather.WeatherAppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.APIKey;
import tk.plogitech.darksky.forecast.ForecastRequest;
import tk.plogitech.darksky.forecast.ForecastRequestBuilder;
import tk.plogitech.darksky.forecast.GeoCoordinates;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

import java.time.Instant;

@Service
public class WeatherService {
    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    private final RestTemplate restTemplate;

    private final String apiKey;

    public WeatherService(RestTemplateBuilder restTemplateBuilder,
                          WeatherAppProperties properties) {
        this.restTemplate = restTemplateBuilder.build();
        this.apiKey = properties.getApi().getDarkskykey();
    }

    @Cacheable("historical")
    public Forecast getHistoricalWeather(Instant pastTime, double lon, double lat) {
        logger.info("Requesting weather forecast for longitude = {} and latitude = {}", lon, lat);
        Forecast forecast = null;
        try {
            ForecastRequest request = new ForecastRequestBuilder()
                    .key(new APIKey(this.apiKey))
                    .time(pastTime)
                    .language(ForecastRequestBuilder.Language.en)
                    .units(ForecastRequestBuilder.Units.us)
                    .exclude(ForecastRequestBuilder.Block.minutely, ForecastRequestBuilder.Block.currently, ForecastRequestBuilder.Block.flags)
                    .extendHourly()
                    .location(new GeoCoordinates(new Longitude(lon), new Latitude(lat))).build();
            DarkSkyJacksonClient client = new DarkSkyJacksonClient();
            forecast = client.forecast(request);
            System.out.println("forecast " + forecast.getDaily().getData().get(0).getSummary());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return forecast;
    }
}
