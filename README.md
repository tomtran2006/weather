# Weather app service 
There are two components for weather app locate in two folders
##### Weather Micro Service   (./server/weather-app)
##### Front end application   (./client/weather-sky-app)

NOTES: There is a completed end to end this micro service and front end are up and running in google cloud platform

Use any browser point to https://angular-app-weather-demo.appspot.com
Then enter location address ( full address or just city , state , e.g Houston , TX)


Also, you could just test the Rest API service only by provide any location address ( full address or just city , state e.g Houston , TX)

     https://tomweatherdemo.appspot.com/api/weather/pastweek/{location-address}

E.g  https://tomweatherdemo.appspot.com/api/weather/pastweek/los angeles , ca/

     Defaul location address  is Los Angeles , Ca"